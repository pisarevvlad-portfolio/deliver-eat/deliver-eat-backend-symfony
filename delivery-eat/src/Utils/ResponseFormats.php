<?php
/**
 * Created by PhpStorm.
 * User: vladislavpisarev
 * Date: 27.05.2018
 * Time: 15:13
 */

namespace App\Utils;

use DateTime;
use Symfony\Component\HttpFoundation\JsonResponse;

abstract class ResponseFormats
{
    /**
     * @param string|null $response_text
     * @param string|null $error
     * @param int $http_status
     * @return JsonResponse
     */
    public static function jsonResponse(?string $response_text, ?string $error, int $http_status = 200): JsonResponse
    {
        if($response_text) $response_text = json_decode($response_text);
        $date = new DateTime();
        $response = [
            'response' => $response_text,
            'meta' => [
                'error' => $error,
                'datetime' => $date->getTimestamp()
            ],
        ];
        return new JsonResponse($response, $http_status);
    }

    /**
     * @param $response_text
     * @param $error
     * @param $count
     * @return JsonResponse
     */
    public static function jsonArrayResponse($response_text, $error, $count): JsonResponse
    {
        $response_text = json_decode($response_text);
        $date = new DateTime();
        $response = [
            'response' => [
                'items' => $response_text,
                'meta' => [
                    'total' => $count
                ]
            ],
            'meta' => [
                'error' => $error,
                'datetime' => $date->getTimestamp()
            ],
        ];
        return new JsonResponse($response);
    }
}
