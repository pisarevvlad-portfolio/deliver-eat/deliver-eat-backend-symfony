<?php

namespace App\Controller;

use App\Entity\OrderEatEntity;
use App\Utils\ResponseFormats;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/api/order-eat")
 * @OA\Tag(name="[Order] Eat Delivery API")
 */
class OrderEatController extends AbstractController
{
    /**
     * Create new order
     *
     * @Route("", methods={"POST"})
     * @OA\RequestBody(
     *     description="Create new order",
     *     @OA\JsonContent(
     *     type="object",
     *     @OA\Property (property="fio", type="string", example="Иванов Иван Иванович"),
     *     @OA\Property (property="phone", type="string", example="791812345678"),
     *     @OA\Property (property="diet_name", type="string", example="Рацион питания", description="Название рациона питания"),
     *     @OA\Property (property="start_delivery", type="string", example="2022-01-01 00:00:00"),
     *     @OA\Property (property="end_delivery", type="string", example="2022-01-18 00:00:00"),
     *     @OA\Property (property="period_delivery", type="integer", example="1", description="Link to article"),
     *     @OA\Property (property="days_eating", type="string", example="{1,2,3,4,5,7}", description="День недели"),
     *     @OA\Property (property="comments", type="string", description="Комментарий к заказу")
     * )
     * )
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function createOrder(ManagerRegistry $doctrine, Request $request): Response
    {
        $response_text = null;
        $error = null;
        $http_response_code = 200;

        $fio = $request->request->get('fio'); // TODO: validator
        $phone = $request->request->get('phone'); // TODO: validator
        $diet = $request->request->get('diet_name');
        $start = $request->request->get('start_delivery');
        $end = $request->request->get('end_delivery');
        $period = $request->request->get('period_delivery'); // TODO: validator (1 - ежедневная, 2 - через день на один день, 3 - через день на два дня)
        $days = explode(',', $request->request->get('days_eating', null)); // TODO: валидатор номеров дней в массиве
        $comments = $request->request->get('comments', null);

        if ($fio && $phone && $diet && $start && $end && $period
            && count($days) > 0 && count($days) <= 7
        ) {
            $start = new \DateTime($start);
            $end = new \DateTime($end);

            if ($end < $start || $start < new \DateTime()) {
                $http_response_code = 400;
                $error = 'Время окончание заказа не может быть меньше времени начала. 
                Также время начала не может быть в прошедшем времени';
            } else {
                $order = new OrderEatEntity();
                $order->setFio($fio);
                $order->setPhoneNumber($phone);
                $order->setDietName($diet);
                $order->setStartDelivery($start);
                $order->setEndDelivery($end);
                $order->setPeriodDelivery($period);
                $order->setDaysEating($days);
                $order->setComments($comments);

                $em = $doctrine->getManager();
                $em->persist($order);
                $em->flush();

                $serializer = new Serializer([new DateTimeNormalizer(), new ObjectNormalizer()], [new JsonEncoder()]);
                $response_text = $serializer->serialize($order, 'json');;
            }
        } else {
            $error = "Не все поля заполнены или заполнены неправильно";
            $http_response_code = 400;
        }
        return ResponseFormats::jsonResponse($response_text, $error, $http_response_code);
    }

    /**
     * Get all orders
     *
     * @Route("/all", methods={"GET"})
     * @param ManagerRegistry $doctrine
     * @return JsonResponse
     */
    public function getAllOrders(ManagerRegistry $doctrine): JsonResponse
    {
        $error = null;

        $orders = $doctrine->getRepository(OrderEatEntity::class)->findBy([], ['created_at' => 'desc']);

        $serializer = new Serializer([new DateTimeNormalizer(), new ObjectNormalizer()], [new JsonEncoder()]);
        $response_text = $serializer->serialize($orders, 'json');
        $count = count($orders);

        return ResponseFormats::jsonArrayResponse($response_text, $error, $count);
    }

    /**
     * Get order by ID
     *
     * @Route("/{id}", methods={"GET"})
     * @OA\Parameter (
     *     name="id",
     *     required=true,
     *     in="path",
     *     example="1"
     * )
     * @param ManagerRegistry $doctrine
     * @param int $id
     * @return JsonResponse
     */
    public function getOrderById(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $response_text = null;
        $error = null;
        $http_response_code = 200;

        $order = $doctrine->getRepository(OrderEatEntity::class)->find($id);


        if ($order) {
            $serializer = new Serializer([new DateTimeNormalizer(), new ObjectNormalizer()], [new JsonEncoder()]);
            $response_text = $serializer->serialize($order, 'json');
        } else {
            $http_response_code = 404;
            $error = "Заказ с ID=".$id." не найден";
        }

        return ResponseFormats::jsonResponse($response_text, $error, $http_response_code);
    }

    /**
     * Get estimated order
     *
     * @Route("/{id}/estimated", methods={"GET"})
     * @OA\Parameter (
     *     name="id",
     *     required=true,
     *     in="path",
     *     example="1"
     * )
     * @param ManagerRegistry $doctrine
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     */
    public function getEstimatedOrder(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $response_text = null;
        $error = null;
        $http_response_code = 200;

        $order = $doctrine->getRepository(OrderEatEntity::class)->find($id);

        if ($order) {
            $serializer = new Serializer([new DateTimeNormalizer(), new ObjectNormalizer()], [new JsonEncoder()]);
            $period_delivery = $order->getPeriodDelivery();
            $interval = $order->getEndDelivery()->diff($order->getStartDelivery())->days;
            $delivery_items = null;

            if ($period_delivery === 1) { // ежедневно
                while ($interval >= 0) {
                    $date_to = date(
                        'Y-m-d'
                        , strtotime($order->getEndDelivery()->format('Y-m-d').' - '.$interval.' days')
                    );
                    if (in_array((int)(new \DateTime($date_to))->format('N'), $order->getDaysEating())) {
                        $delivery_items[] = [
                            'delivery_date' => $date_to,
                            'delivery_count' => 1
                        ];
                    }
                    $interval--;
                }
            } elseif ($period_delivery === 2) { // через день на один день
                // TODO: нужно узнать логику, пока не понятно как это должно работать
            } elseif ($period_delivery === 3) { // через два дня на два дня
                while ($interval >= 0) {
                    $date_to = date(
                        'Y-m-d'
                        , strtotime($order->getEndDelivery()->format('Y-m-d').' - '.$interval.' days')
                    );
                    if (in_array((int)(new \DateTime($date_to))->format('N'), $order->getDaysEating())) {
                        $date_next_to = date(
                            'Y-m-d'
                            , strtotime($date_to.' + 1 days')
                        );
                        $need_deliver_portion = 1;
                        if (
                            in_array((int)(new \DateTime($date_next_to))->format('N'), $order->getDaysEating())
                            && new \DateTime($date_next_to) <= $order->getEndDelivery()
                        ) {
                            $need_deliver_portion = 2;
                            $interval--;
                        }
                        $delivery_items[] = [
                            'delivery_date' => $date_to,
                            'delivery_count' => $need_deliver_portion
                        ];
                    }
                    $interval--;
                }
            }

            $response_text = [
                'order' => $order,
                'delivery' => $delivery_items,
                'need_deliver' => count($delivery_items)
            ];

            $response_text = $serializer->serialize($response_text, 'json');
        } else {
            $http_response_code = 404;
            $error = "Заказ с ID=".$id." не найден";
        }

        return ResponseFormats::jsonResponse($response_text, $error, $http_response_code);
    }
}
