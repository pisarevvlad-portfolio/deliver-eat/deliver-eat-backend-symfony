<?php

namespace App\Entity;

use App\Repository\OrderEatEntityRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrderEatEntityRepository::class)]
class OrderEatEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 128)]
    private $fio;

    #[ORM\Column(type: 'string', length: 32)]
    private $phoneNumber;

    #[ORM\Column(type: 'datetime')]
    private $created_at;

    #[ORM\Column(type: 'string', length: 64)]
    private $diet_name;

    #[ORM\Column(type: 'datetime')]
    private $start_delivery;

    #[ORM\Column(type: 'datetime')]
    private $end_delivery;

    #[ORM\Column(type: 'integer')]
    private $period_delivery;

    #[ORM\Column(type: 'array')]
    private $days_eating = [];

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $comments;

    public function __construct()
    {
        $this->created_at = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFio(): ?string
    {
        return $this->fio;
    }

    public function setFio(string $fio): self
    {
        $this->fio = $fio;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getDietName(): ?string
    {
        return $this->diet_name;
    }

    public function setDietName(string $diet_name): self
    {
        $this->diet_name = $diet_name;

        return $this;
    }

    public function getStartDelivery(): ?\DateTimeInterface
    {
        return $this->start_delivery;
    }

    public function setStartDelivery(\DateTimeInterface $start_delivery): self
    {
        $this->start_delivery = $start_delivery;

        return $this;
    }

    public function getEndDelivery(): ?\DateTimeInterface
    {
        return $this->end_delivery;
    }

    public function setEndDelivery(\DateTimeInterface $end_delivery): self
    {
        $this->end_delivery = $end_delivery;

        return $this;
    }

    public function getPeriodDelivery(): ?int
    {
        return $this->period_delivery;
    }

    public function setPeriodDelivery(int $period_delivery): self
    {
        $this->period_delivery = $period_delivery;

        return $this;
    }

    public function getDaysEating(): ?array
    {
        return $this->days_eating;
    }

    public function setDaysEating(array $days_eating): self
    {
        $this->days_eating = $days_eating;

        return $this;
    }

    public function getComments(): ?string
    {
        return $this->comments;
    }

    public function setComments(?string $comments): self
    {
        $this->comments = $comments;

        return $this;
    }
}
