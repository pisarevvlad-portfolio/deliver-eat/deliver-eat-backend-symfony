# deliver-eat-backend-symfony

Для запуска приложения необходимо настроить .env файл внутри директории delivery-eat

`DATABASE_URL` - адрес доступа к БД, например: `mysql://user-name:user-password@127.0.0.1:3306/db-schema?serverVersion=5.7`

`DATABASE_YANDEX_PEM_SSL_CERT` - адрес к сертификату MySQL, например: `/var/www/domain/CA.pem`

После необходимо внутри директории проекта - `delivery-eat` - установить все зависимости, командой `composer install`

Для запуска использовать команду `symfony server:start` - если не лоокально то смотреть настройку в интренете под свой Web-сервер

Если надо разрешить CORS запросы то необходимо настроить `CORS_ALLOW_ORIGIN` в файле .env

